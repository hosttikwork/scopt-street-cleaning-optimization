export default {
  changeTheme: (state, newThemeName) => {
    state.theme = newThemeName;
  },
};
